<?php
// Created on: <30-Jul-2009 14:14 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates
$http = eZHTTPTool::instance();
$Module = $Params["Module"];

include_once('kernel/common/template.php');
$tpl = eZTemplate::factory();

$objectId = $Params['ObjectID'];
$object = eZContentObject::fetch( $objectId );

if ( !is_object( $object ) )
{
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
}

$value = ''; 
if ( $Module->isCurrentAction('Create') )
{
    try {
        $name = $Module->actionParameter( 'Name' );
        $languageCode = $Module->actionParameter( 'Language' );
        $value = $Module->actionParameter( 'Value' );
        ISMetaData::create( $objectId, $name, $languageCode, $value );
        eZContentCacheManager::clearObjectViewCache( $objectId );
//         foreach( $object->assignedNodes() as $node ) //Commented for excessiv time usage on root nodes... Please clear all Cache after settings metadatas to be sure heritence is taken. 
//         {
//             eZContentObjectTreeNode::clearViewCacheForSubtree( $node );
//         }
        return $Module->redirect( 'content', 'view', array( 'full', $object->attribute( 'main_node_id' ) ), null, false, 'metadata' );
    } catch (Exception $e) {
        $tpl->setVariable( 'error_message', $e->getMessage() );
        $tpl->setVariable( 'name', $name );
        $tpl->setVariable( 'language_code', $languageCode );
        $tpl->setVariable( 'value', $value );
    }
}
elseif ( $http->hasPostVariable( 'CustomActionButton' ) )
{
    $name = $http->postVariable( 'Name' );
    $languageCode = $http->postVariable( 'Language' );
    $value = $http->postVariable( 'Value' );
    $customAction = $http->postVariable( 'CustomActionButton' );
    $tpl->setVariable( 'name', $name );
    $tpl->setVariable( 'language_code', $languageCode );
    $tpl->setVariable( 'value', $value );
    if ( isset( $customAction['0_disable_editor'] ) )
    {
        ISOEMetaDataXMLInput::setIsEditorEnabled( false );
    }
    elseif ( isset( $customAction['0_enable_editor'] ) )
    {
        ISOEMetaDataXMLInput::setIsEditorEnabled( true );
    }
}
elseif ( $Module->isCurrentAction('Discard') )
{
    return $Module->redirect( 'content', 'view', array( 'full', $object->attribute( 'main_node_id' ) ), null, false, 'metadata' );
}

$tpl->setVariable( 'object', $object );
if ( empty( $value ) )
{
    $parser = new eZXMLInputParser();
    $doc = $parser->createRootNode();
    $value = eZXMLTextType::domString( $doc );
}
else
{
    $parser = new eZOEInputParser();
    $doc = $parser->process( $value );
    $value = eZXMLTextType::domString( $doc );
}

$inputHandler = new ISOEMetaDataXMLInput( $value );
$tpl->setVariable( 'xml_input_handler', $inputHandler );

$localeList = eZContentLanguage::fetchList();
$tpl->setVariable( 'locale_list', $localeList);
$path = array( array( 'url' => 'metadata/create',
                      'text' => ezpI18n::tr( 'ismetadata/metadata/create', 'Create metadata...' ) ) );
$Result['content'] = $tpl->fetch( 'design:metadata/create.tpl' );
$Result['path'] = $path;
