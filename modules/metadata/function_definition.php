<?php
// Created on: <03-Aug-2009 15:07 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates

$FunctionList = array();

$FunctionList['custom'] = array( 'name' => 'custom',
                                 'operation_types' => array( 'read' ),
                                 'call_method' => array( 'class' => 'ISMetaDataFunctionCollection',
                                                         'method' => 'fetchCustomMetadata' ),
                                 'parameter_type' => 'standard',
                                 'parameters' => array( array( 'name' => 'object_id',
                                                               'type' => 'integer',
                                                               'default' => false,
                                                               'required' => true ) ) );
$FunctionList['list'] =   array( 'name' => 'custom',
                                 'operation_types' => array( 'read' ),
                                 'call_method' => array( 'class' => 'ISMetaDataFunctionCollection',
                                                         'method' => 'fetchMetadataByObjectId' ),
                                 'parameter_type' => 'standard',
                                 'parameters' => array( array( 'name' => 'object_id',
                                                               'type' => 'integer',
                                                               'default' => false,
                                                               'required' => true ),
                                                        array( 'name' => 'language_code',
                                                               'type' => 'string',
                                                               'default' => false,
                                                               'required' => true ) ) );

?>
