<?php
// Created on: <30-Jul-2009 15:17 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates

$http = eZHTTPTool::instance();
$Module = $Params["Module"];

include_once('kernel/common/template.php');
$tpl = eZTemplate::factory();

$objectId = $Params['ObjectID'];
$object = eZContentObject::fetch( $objectId );

if ( $Module->hasActionParameter( 'RemoveList' ) )
{
    $removeList = $Module->actionParameter( 'RemoveList' );
}
elseif ( $http->hasSessionVariable( 'ISMetaDataRemoveList' ) )
{
    $removeList = $http->sessionVariable( 'ISMetaDataRemoveList' );
    $http->setSessionVariable( 'ISMetaDataRemoveList', '' );
}
else
{
    $removeList = array();
}

$metadataList = array();
foreach( $removeList as $val )
{
    $exploded = explode( '/', $val );
    $languageCode = array_shift( $exploded );
    $name = implode( '/', $exploded );
    $metadata = ISMetaData::fetchExisting( $objectId, $name, $languageCode );
    if ( $metadata )
    {
        $metadataList[] = $metadata;
    }
}

if ( $Module->isCurrentAction('Remove') )
{
    if ( $Module->hasActionParameter( 'Confirm' ) )
    {
        $confirm = $Module->actionParameter( 'Confirm' );
    }
    else
    {
        $confirm = false;
    }
    if ( $confirm ) {
        try {
            ISMetaData::removeList( $metadataList );
            eZContentCacheManager::clearContentCache($objectId);
            return $Module->redirect( 'content', 'view', array( 'full', $object->attribute( 'main_node_id' ) ), null, false, 'metadata' );
        } catch (Exception $e) {
            $tpl->setVariable( 'error_message', $e->getMessage() );
        }
    }
}
else
{
    return $Module->redirect( 'content', 'view', array( 'full', $object->attribute( 'main_node_id' ) ), null, false, 'metadata' );
}

$tpl->setVariable( 'object', $object );
$tpl->setVariable( 'metadata_list', $metadataList );

$http->setSessionVariable( 'ISMetaDataRemoveList', $removeList );

$path = array( array( 'url' => 'metadata/remove',
                      'text' => ezpI18n::tr( 'ismetadata/metadata/remove', 'Remove metadata...' ) ) );
$Result['content'] = $tpl->fetch( 'design:metadata/remove.tpl' );
$Result['path'] = $path;
