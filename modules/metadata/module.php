<?php
// Created on: <30-Jul-2009 14:02 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates
$Module = array( 'name' => 'IS Metadata' );

$ViewList = array();
$ViewList['create'] = array(  'script' => 'create.php',
                              'ui_context' => 'administration',
                              'functions' => array( 'create' ),
                              'single_post_actions' => array( 'CreateButton' => 'Create', 'DiscardButton' => 'Discard' ),
                              'post_action_parameters' => array( 'Create' => array( 'Name' => 'Name', 'Language' => 'Language', 'Value' => 'Value' ) ),
                              'params' => array( 'ObjectID' ) );
$ViewList['edit'] = array(    'script' => 'edit.php',
                              'ui_context' => 'administration',
                              'functions' => array( 'edit' ),
                              'single_post_actions' => array( 'EditButton' => 'Edit', 'DiscardButton' => 'Discard' ),
                              'post_action_parameters' => array( 'Edit' => array( 'Value' => 'Value', 'Value2' => 'Metadata_data_text_0' ) ),
                              'params' => array( 'ObjectID', 'Name', 'Language' ) );
$ViewList['remove'] = array(  'script' => 'remove.php',
                              'ui_context' => 'administration',
                              'functions' => array( 'remove' ),
                              'single_post_actions' => array( 'RemoveButton' => 'Remove' ),
                              'post_action_parameters' => array( 'Remove' => array( 'RemoveList' => 'RemoveList', 'Confirm' => 'Confirm' ) ),
                              'params' => array( 'ObjectID' ) );
$FunctionList = array();
$FunctionList['create'] = array();
$FunctionList['edit'] = array();
$FunctionList['remove'] = array();
?>
