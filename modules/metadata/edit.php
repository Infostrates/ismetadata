<?php
// Created on: <30-Jul-2009 14:44 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates

$http = eZHTTPTool::instance();
$Module = $Params["Module"];

include_once('kernel/common/template.php');
$tpl = eZTemplate::factory();

$objectId = $Params['ObjectID'];
$languageCode = $Params['Language'];
$name = $Params['Name'];
$object = eZContentObject::fetch( $objectId );
$metadataObject = ISMetaData::fetchExisting( $objectId, $name, $languageCode );

if ( !$metadataObject )
{
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
}

$value = $metadataObject->attribute( 'value' );
$tpl->setVariable( 'name', $metadataObject->attribute( 'name' ) );
$tpl->setVariable( 'language_code', $metadataObject->attribute( 'language_code' ) );
$tpl->setVariable( 'value', $value );
if ( $Module->isCurrentAction('Edit') )
{
    try {
        if ( $metadataObject->isXmlBlock() )
        {
            $value = $Module->actionParameter( 'Value2' );
        }
        else
        {
            $value = $Module->actionParameter( 'Value' );
        }
        $metadataObject->setCustomContent( $value );
        eZContentCacheManager::clearObjectViewCache( $objectId );
        foreach( $object->assignedNodes() as $node )
        {
            eZContentObjectTreeNode::clearViewCacheForSubtree( $node );
        }
        return $Module->redirect( 'content', 'view', array( 'full', $object->attribute( 'main_node_id' ) ), null, false, 'metadata' );
    } catch (Exception $e) {
        $tpl->setVariable( 'error_message', $e->getMessage() );
        $tpl->setVariable( 'value', $value );
        if ( empty( $value ) )
        {
            $parser = new eZXMLInputParser();
            $doc = $parser->createRootNode();
            $value = eZXMLTextType::domString( $doc );
        }
        else
        {
            $parser = new eZOEInputParser();
            $doc = $parser->process( $value );
            $value = eZXMLTextType::domString( $doc );
        }
    }
}
elseif ( $http->hasPostVariable( 'CustomActionButton' ) )
{
    if ( $metadataObject->isXmlBlock() )
    {
        if ( $http->hasPostVariable( 'Metadata_data_text_0' ) )
        {
            $value = $http->postVariable( 'Metadata_data_text_0' );
        }
        else
        {
            $value = $Module->actionParameter( 'Value2' );
        }
        if ( empty( $value ) )
        {
            $parser = new eZXMLInputParser();
            $doc = $parser->createRootNode();
            $value = eZXMLTextType::domString( $doc );
        }
        else
        {
            $parser = new eZOEInputParser();
            $doc = $parser->process( $value );
            $value = eZXMLTextType::domString( $doc );
        }
    }
    else
    {
        $value = $Module->actionParameter( 'Value' );
    }
    $customAction = $http->postVariable( 'CustomActionButton' );
    if ( isset( $customAction['0_disable_editor'] ) )
    {
        ISOEMetaDataXMLInput::setIsEditorEnabled( false );
    }
    elseif ( isset( $customAction['0_enable_editor'] ) )
    {
        ISOEMetaDataXMLInput::setIsEditorEnabled( true );
    }
    $tpl->setVariable( 'value', $value );
}
elseif ( $Module->isCurrentAction('Discard') )
{
    return $Module->redirect( 'content', 'view', array( 'full', $object->attribute( 'main_node_id' ) ), null, false, 'metadata' );
}

$tpl->setVariable( 'object', $object );

$inputHandler = new ISOEMetaDataXMLInput( $value );
$tpl->setVariable( 'xml_input_handler', $inputHandler );

$locale = eZContentLanguage::fetchByLocale( $languageCode );
$tpl->setVariable( 'language_name', $locale->attribute( 'name' ) );


$path = array( array( 'url' => 'metadata/edit',
                      'text' => ezpI18n::tr( 'ismetadata/metadata/edit', 'Edit metadata...' ) ) );
$Result['content'] = $tpl->fetch( 'design:metadata/edit.tpl' );
$Result['path'] = $path;
