<?php
// Created on: <27-Jul-2009 14:38 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates

/**
 * Classe d'opérateurs de template de l'extension.
 * 
 * Attention, ces opérateurs ne peuvent être utilisé que dans le pagelayout!
 * @author Marc
 *
 */
class ISMetaDataOperator
{

    public function operatorList()
    {
        return array( 'ismetadata' );
    }

    public function namedParameterPerOperator()
    {
        return true;
    }

    public function namedParameterList()
    {
        return array( 'ismetadata' => array( 'name' => array( 'type' => 'string',
                                                              'default' => false,
                                                              'required' => true ) ) );
    }
    
    public function modify( $tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue, $namedParameters )
    {
        switch ( $operatorName )
        {
            case 'ismetadata' :
            {
                $operatorValue = ISMetaData::currentByName( $namedParameters['name'] )->content();
                return;
            }
        }
    }
}