<?php
// Created on: <27-Jul-2009 14:15 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates

$eZTemplateOperatorArray = array();
$eZTemplateOperatorArray[] = array( 'script' => 'extension/ismetadata/autoloads/ismetadataoperator.php',
                                    'class' => 'ISMetaDataOperator',
                                    'operator_names' => array( 'ismetadata' ) );
?>