<?php
// Created on: <17-Aug-2009 16:02 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates
//
// Run this script with ezexec

$db = eZDB::instance();
$db->begin();
include( 'extension/ismetadata/scripts/create_db.php' );
$result = $db->arrayQuery( 'SELECT * FROM fezmeta_data' );
foreach( $result as $line )
{
    $db->query( 'INSERT INTO ismetadata( `contentobject_id`, `name`, `value` ) 
    VALUES( "'.$db->escapeString( $line['contentobject_id'] ).'", "'.$db->escapeString( $line['meta_name'] ).'", "'.$db->escapeString( $line['meta_value'] ).'" )');
}

$db->commit();