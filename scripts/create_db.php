<?php
// Created on: <17-Aug-2009 14:50 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates
//
// Run this script with ezexec

$db = eZDB::instance();
$db->begin();
$db->query( 'CREATE TABLE IF NOT EXISTS `ismetadata` (
  `contentobject_id` int(11) NOT NULL,
  `language_code` varchar(16) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`contentobject_id`,`language_code`,`name`)
) DEFAULT CHARSET=utf8;' );
$db->commit();