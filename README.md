#ISMetadata

ISMetaData est une extension permettant de mieux gérer les métadatas générés par chacune de vos pages.

Elle vous permet de récuperer au niveaux de vos content/view/full (et page similaire) de chacun de vos objets :

- Titre personnalisée (ou par défaut, celui généré par eZ)
- Description personnalisée (ou par défaut, celle généré par eZ)
- Keywords personnalisée (ou par défaut, ceux générés par eZ) plus ceux présent en attribut de datatype 'Tag' de vos objets 
- Baseline ( avec gestion de contenu eZOE)
