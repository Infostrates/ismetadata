{* DO NOT EDIT THIS FILE! Use an override template instead. *}
{set-block scope=global variable=cache_ttl}0{/set-block}
{if and( ezini_hasvariable( concat( 'MetaData_', $name ), 'XmlBlock', 'ismetadata.ini' ), ezini( concat( 'MetaData_', $name ), 'XmlBlock', 'ismetadata.ini' )|downcase|eq( 'enabled' ) )}
    {def $xmlblocktype = true()}
{else}
    {def $xmlblocktype = false()}
{/if}
{if $error_message}
	<div class="message-error">
	  <h2>{'Invalid metadata...'|i18n( 'ismetadata/metadata/edit' )}</h2>
	  {$error_message|wash}
	</div>
{/if}
<form method="post" action={concat( 'metadata/edit/', $object.id, '/', $name, '/', $language_code )|ezurl}>
	<div class="context-block">
	
		{* DESIGN: Header START *}<div class="box-header"><div class="box-tc"><div class="box-ml"><div class="box-mr"><div class="box-tl"><div class="box-tr">
		
		<h1 class="context-title">{'Edit Metadata...'|i18n('ismetadata/metadata/edit')}</h1>
		
		{* DESIGN: Mainline *}<div class="header-mainline"></div>
		
		{* DESIGN: Header END *}</div></div></div></div></div></div>
			
		{* DESIGN: Content START *}<div class="box-ml"><div class="box-mr"><div class="box-content">
		<div class="context-attributes">
            <div class="block">
                <label>{'Name'|i18n('ismetadata/metadata/create')}</label>
                <i>{$name|wash}</i>
            </div>
			<div class="block">
				<label>{'Language'|i18n('ismetadata/metadata/create')}:</label>
				<i>{$language_name|wash}</i>
			</div>
			
			<div class="block">
				<label>{'Value'|i18n( 'ismetadata/metadata/create' )}:</label>
				{if $xmlblocktype|not}
					<div id="metadata_simple_div">
					    <input type="text" id="Metadata_simple_text_name" name="Value" value="{$value|wash}" size="50" />
					</div>
				{else}
					<div id="metadata_xml_div">
					    <div id="Metadata_xml_div">
						    {def $dummy_attribute = hash( 'content', hash( 'input', $xml_input_handler ),
						                                  'contentclass_attribute', hash( 'data_int1', 5 ),
						                                  'language_code', ezini( 'RegionalSettings', 'Locale', 'site.ini'),
						                                  'contentobject_id', $object.id,
						                                  'version', $object.current_version,
						                                  'id', 0 )}
						    {include uri="design:content/datatype/edit/ezxmltext_ezoe.tpl" attribute=$dummy_attribute attribute_base='Metadata'}
						    {undef $dummy_attribute}
						</div>
					</div>
				{/if}
			</div>
		</div>
		{* DESIGN: Content END *}</div></div></div>
		
			
		<div class="controlbar">
			{* DESIGN: Control bar START *}<div class="box-bc"><div class="box-ml"><div class="box-mr"><div class="box-tc"><div class="box-bl"><div class="box-br">
			<div class="block">
					<input class="button" type="submit" value="{'Edit'|i18n('ismetadata/metadata/edit')}" name="EditButton" />
					<input class="button" type="submit" value="{'Cancel'|i18n('ismetadata/metadata/create')}" name="DiscardButton" />
			</div>
			{* DESIGN: Control bar END *}</div></div></div></div></div></div>		
		</div>
	</div>
</form>
{undef $name_list}