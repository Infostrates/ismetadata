{debug-accumulator id='ismetadata_metadatas' name='Metadata Template'}
{def $origin_translations = hash( 'custom', 'Custom'|i18n( 'metadata/metadatas' ),
                                  'attribute', 'Attribute'|i18n( 'metadata/metadatas' ), 
                                  'default', 'Default'|i18n( 'metadata/metadatas' ) )
     $metadatas=fetch( 'metadata', 'custom', hash( 'object_id', $node.object.id ) )
     $can_edit=fetch( 'user', 'has_access_to', hash( 'module', 'metadata', 'function', 'edit' ) )
     $can_create=fetch( 'user', 'has_access_to', hash( 'module', 'metadata', 'function', 'create' ) )
     $can_remove=fetch( 'user', 'has_access_to', hash( 'module', 'metadata', 'function', 'remove' ) )
     $prefered_view_language_code = ezpreference( 'admin_navigation_metadatas_language' )}
{if $metadatas}
    {def $metadatas_count=$metadatas|count}
{else}
    {def $metadatas_count=0}
{/if}

{if $prefered_view_language_code}
    {def $view_language_code=$prefered_view_language_code}
{else}
    {def $view_language_code=fetch( 'content', 'locale' ).locale_code}
{/if}
     

{if $can_remove}
    <form method="post" action={concat( 'metadata/remove/', $node.object.id)|ezurl}>
{/if}
<div class="context-block">

{* DESIGN: Content START *}{* DESIGN: Content START *}<div class="box-bc"><div class="box-ml"><div class="box-mr"><div class="box-bl"><div class="box-br"><div class="box-content">

<div class="block">
<fieldset>
<legend>{'Existing custom metadatas'|i18n( 'metadata/metadatas' )}</legend>
{if $metadatas}
	<table class="list" cellspacing="0">
	<tr>
	    {if $can_remove}<th class="tight"><img src={'toggle-button-16x16.gif'|ezimage} alt="{'Invert selection.'|i18n( 'design/admin/node/view/full' )}" title="{'Invert selection.'|i18n( 'design/admin/node/view/full' )}" onclick="ezjs_toggleCheckboxes( document.translationsform, 'RemoveList[]' ); return false;"/></th>{/if}
	    <th class="tight">{'Name'|i18n( 'metadata/metadatas' )}</th>
	    <th class="tight">{'Language'|i18n( 'metadata/metadatas' )}</th>
	    <th>{'Value'|i18n( 'metadata/metadatas' )}</th>
	    {if $can_edit}<th class="tight">&nbsp;</th>{/if}
	</tr>
	{foreach $metadatas as $item sequence array( bglight, bgdark ) as $sequence}
	<tr class="{$sequence}">
	{if $can_remove}
		<td>
		    <input type="checkbox" name="RemoveList[]" value="{concat( $item.language_code, '/', $item.name )}" />
		</td>
	{/if}
	    <td>
	        {$item.public_name|wash}
	    </td>
	    <td>
	        <img src="{$item.language_code|flag_icon}" alt="{$item.language_code}" /> {$item.language_name|wash}
	    </td>
	    <td>
	        {$item.custom_content}
	    </td>
	{if $can_remove}
	    <td>
	        <a href={concat( 'metadata/edit/', $node.object.id, '/', $item.name, '/', $item.language_code )|ezurl}><img src={'edit.gif'|ezimage} alt="{'Edit this metadata'|i18n( 'metadata/metadatas' )|wash}" title="{'Edit this metadata'|i18n( 'metadata/metadatas' )}" /></a>
	    </td>
	{/if}
	</tr>
	{/foreach}
	</table>
{else}
    {'No custom metadatas yet!'|i18n( 'metadata/metadatas' )}
{/if}
<div class="block">
<div class="button-left">
{if $can_remove}
    {if $metadatas_count|gt( 0 )}
    <input class="button" type="submit" name="RemoveButton" value="{'Remove selected'|i18n( 'design/admin/node/view/full' )}" title="{'Remove selected metadatas from the list above.'|i18n( 'metadata/metadatas' )}" />
    {else}
    <input class="button-disabled" type="submit" name="RemoveButton" value="{'Remove selected'|i18n( 'design/admin/node/view/full' )}" title="{'There is no removable metadata.'|i18n( 'metadata/metadatas' )}" disabled="disabled" />
    {/if}
{else}
    <input class="button-disabled" type="submit" name="" value="{'Remove selected'|i18n( 'design/admin/node/view/full' )}" disabled="disabled" title="{'You cannot remove any metadata because you do not have permission.'|i18n( 'metadata/metadatas' )}" />
{/if}
</div>

<div class="button-right">
{if $can_create}
    <input class="button" type="button" value="{'Create metadata'|i18n( 'metadata/metadatas' )}" title="{'Create a metadata for this object.'|i18n( 'metadata/metadatas' )}" onclick="window.location.href = {concat( 'metadata/create/', $node.object.id)|ezurl( 'single' )};" />
{else}
    <input class="button-disabled" type="submit" name="" value="{'Create metadata'|i18n( 'metadata/metadatas' )}" disabled="disabled" title="{'You cannot create new metadata because you do not have permission.'|i18n( 'metadata/metadatas' )}" />
{/if}
</div>

<div class="break"></div>
</div>
</fieldset>
</div>

{* DESIGN: Content END *}</div></div></div></div></div></div>

</div>

</form>
{undef $metadatas $metadatas_count}
{/debug-accumulator}