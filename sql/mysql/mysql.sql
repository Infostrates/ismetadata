CREATE TABLE `ismetadata` (
`contentobject_id` INT NOT NULL ,
`language_code` VARCHAR( 16 ) NOT NULL ,
`name` VARCHAR( 32 ) NOT NULL ,
`value` TEXT NOT NULL ,
PRIMARY KEY ( `contentobject_id` , `language_code` , `name` )
)