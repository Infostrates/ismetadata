<?php /* #?ini charset="utf-8"?

#Configuration générale de l'extension
[MetaData]
#Liste les métadata disponible, vous etes libre d'en ajouter à votre guise.
#Note: seul le metadata "title" possède un traitement spécial, le reste
AvailablesMetaData[]
AvailablesMetaData[]=title
AvailablesMetaData[]=description
AvailablesMetaData[]=keywords
AvailablesMetaData[]=robots
AvailablesMetaData[]=baseline

#Configuration spéciale relative au métadata title : partie obligatoire
[MetaDataTitleDefaultActions]
#Valeurs de AddPath : disabled, enabled(dans tout les cas), onAttribute(par défaut et par attribut), onDefault(par défaut)
AddPath=onDefault
#Valeurs de AddMode : prepend, append
AddMode=prepend
#Valeurs de ReversedPath : true, false
ReversedPath=true
PathSeparator= / 
MergeString= - 
PathOffset=0
PathLimit=0


#Bloc de référence contenant les valeurs par défaut 
#(mis a part MergeString (' ') ainsi que Name et DefaultIniVariable qui n'ont pas de valeurs par défaut)
[MetaData_description]
Name=Description
XmlBlock=disabled
#DefaultIniVariable correspond au chemin logique pour atteindre la valeur voulue, séparé par des /
#Exemple : site.ini/SiteSettings/MetaDataArray/author pour l'index author de la variable MetaDataArray du bloc SiteSettings de l'ini site.ini
#Note : L'index n'est bien sur utilisable que dans le cas d'un tableau
#Note2 : rien de vous empeche d'ajouter un bloc dans ce fichier et de pointer dessus pour ajouter vos propre variables par défaut
#DefaultIniVariable=site.ini/SiteSettings/MetaDataArray/description
#Valeurs de Action : override(écrase ceux par défaut), prepend (ajoute avant la valeur par défaut),
#                    append( ajoute apres la valeur custom)
Action=override
#Note : SourceAttributesDataType et SourceAttributeName overrident le settings XmlBlock.
#       C'est l'attribut content de l'attribut qui sera récupéré.
#Note2 : En cas de présence de plusieurs attributs du datatype entré, leurs contenu sera concaté avec comme glue : MergeString 
SourceAttributesDataType=
SourceAttributeName=
#Valeurs de SourceAttributeAction: prepend (ajoute avant la valeur custom), append( ajoute apres la valeur custom),  
#                                  fallback (en cas d'absence de custom), override (lorsque l'attribut est défini, override le custom) 
SourceAttributeAction=fallback
MergeString= - 
Heritage=disabled

[MetaData_title]
Name=Title
Action=prepend
MergeString= - 
DefaultIniVariable=site.ini/SiteSettings/SiteName

[MetaData_keywords]
Name=Keywords
SourceAttributesDataType=ezkeyword
#DefaultIniVariable=site.ini/SiteSettings/MetaDataArray/keywords
MergeString=,
Action=prepend

[MetaData_robots]
Name=Robots

[MetaData_baseline]
Name=Baseline
XmlBlock=enabled
*/ ?>
