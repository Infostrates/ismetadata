<?php
// Created on: <31-Jul-2009 09:36 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates

class ISOEMetaDataXMLInput extends eZOEXMLInput
{

    public function __construct( $value )
    {
        $dummyContentObjectAttribute = new eZContentObjectAttribute( array( 'id' => 0, 'data_text' => $value, 'contentobject_id' => eZContentObjectTreeNode::fetch( eZINI::instance( 'content.ini' )->variable( 'NodeSettings', 'RootNode' ) )->attribute( 'contentobject_id' ) ) );
        parent::__construct( $value, false, $dummyContentObjectAttribute );
    }
    
    function getEditorLayoutSettings()
    {    
        if ( $this->editorLayoutSettings === null )
        {
            $oeini = eZINI::instance( 'ezoe.ini' );
            $xmlini = eZINI::instance( 'ezxml.ini' );

            // get global layout settings
            $editorLayoutSettings = self::getEditorGlobalLayoutSettings();

            // get custom layout features, works only in eZ Publish 4.1 and higher
            $buttonPreset = '';
            $buttonPresets = $xmlini->hasVariable( 'TagSettings', 'TagPresets' ) ? $xmlini->variable( 'TagSettings', 'TagPresets' ) : array();

            if( $buttonPreset && isset( $buttonPresets[$buttonPreset] ) )
            {
                if ( $oeini->hasSection( 'EditorLayout_' . $buttonPreset ) )
                {
                    if ( $oeini->hasVariable( 'EditorLayout_' . $buttonPreset , 'Buttons' ) )
                        $editorLayoutSettings['buttons'] = $oeini->variable( 'EditorLayout_' . $buttonPreset , 'Buttons' );

                    if ( $oeini->hasVariable( 'EditorLayout_' . $buttonPreset , 'ToolbarLocation' ) )
                        $editorLayoutSettings['toolbar_location'] = $oeini->variable( 'EditorLayout_' . $buttonPreset , 'ToolbarLocation' );

                    if ( $oeini->hasVariable( 'EditorLayout_' . $buttonPreset , 'PathLocation' ) )
                        $editorLayoutSettings['path_location'] = $oeini->variable( 'EditorLayout_' . $buttonPreset , 'PathLocation' );
                }
                else
                {
                    eZDebug::writeWarning( 'Undefined EditorLayout : EditorLayout_' . $buttonPreset, __METHOD__ );
                }
            }

            $contentini = eZINI::instance( 'content.ini' );
            $tags = $contentini->variable('CustomTagSettings', 'AvailableCustomTags' );
            $hideButtons = array();
            $showButtons = array();

            // filter out custom tag icons if the custom tag is not enabled
            if ( !in_array('underline', $tags ) )
                $hideButtons[] = 'underline';

            if ( !in_array('sub', $tags ) )
                $hideButtons[] = 'sub';

            if ( !in_array('sup', $tags ) )
                $hideButtons[] = 'sup';

            if ( !in_array('pagebreak', $tags ) )
                $hideButtons[] = 'pagebreak';

            // filter out relations buttons if user dosn't have access to relations
            if ( !$this->currentUserHasAccess( 'relations' ) )
            {
                $hideButtons[] = 'image';
                $hideButtons[] = 'object';
                $hideButtons[] = 'file';
                $hideButtons[] = 'media';
            }

            // filter out align buttons on eZ Publish 4.0.x
            if ( $this->eZPublishVersion < 4.1 )
            {
                $hideButtons[] = 'justifyleft';
                $hideButtons[] = 'justifycenter';
                $hideButtons[] = 'justifyright';
                $hideButtons[] = 'justifyfull';
            }
             
            foreach( $editorLayoutSettings['buttons'] as $button )
            {
                if ( !in_array( $button, $hideButtons ) )
                    $showButtons[] = trim( $button );
            }

            $editorLayoutSettings['buttons'] = $showButtons;
            $this->editorLayoutSettings = $editorLayoutSettings;
        }
        return $this->editorLayoutSettings;
    }
    
    /**
     * Validates and parses input using {@link eZOEInputParser::process}
     * and saves data if valid.
     * 
     * Throw exception
     *
     * @param $http eZHTTPTool 
     * @param $base string 
     * @param $metadata ISMetaData
     * @return void
     */
    public static function validateAndStoreInput( $value, $metadata )
    {
        if ( !self::isEditorEnabled() )
        {
            return self::validateAndStoreNoEditorInput( $value, $metadata );
        }
        $value = preg_replace( '#<!--.*?-->#s', '', $value ); // remove HTML comments
        $value = str_replace( "\r", '', $value);

        if ( self::browserSupportsDHTMLType() === 'Trident' ) // IE
        {
            $value = preg_replace( "/[\n\t]/", '', $value);
        }
        else
        {
            $value = preg_replace( "/[\n\t]/", ' ', $value);
        }

        //eZDebug::writeDebug( $value, __METHOD__ );

        include_once( 'extension/ezoe/ezxmltext/handlers/input/ezoeinputparser.php' );

        $parser = new eZOEInputParser();

        $document = $parser->process( $value );
        
        if ( !is_object( $document ) )
        {
            throw new Exception( ezpI18n::tr( 'ismetadata/oemetadataxmlinput', 'Value is malformated, some tag may be missing!' ) );
        }
        
        // Remove last empty paragraph (added in the output part)
        $parent = $document->documentElement;
        $lastChild = $parent->lastChild;
        while( $lastChild && $lastChild->nodeName !== 'paragraph' )
        {
            $parent = $lastChild;
            $lastChild = $parent->lastChild;
        }

        if ( $lastChild && $lastChild->nodeName === 'paragraph' )
        {
            $textChild = $lastChild->lastChild;
            // $textChild->textContent == " " : string(2) whitespace in Opera
            if ( !$textChild ||
                 ( $lastChild->childNodes->length == 1 &&
                   $textChild->nodeType == XML_TEXT_NODE &&
                   ( $textChild->textContent == " " || $textChild->textContent == ' ' ||$textChild->textContent == '' ) ) )
            {
                $parent->removeChild( $lastChild );
            }
        }

        //$oeini = eZINI::instance( 'ezoe.ini' );

        /*
        // Update URL-object links
        $urlIDArray = $parser->getUrlIDArray();
        if ( count( $urlIDArray ) > 0 )
        {
            self::updateUrlObjectLinks( $metadata, $urlIDArray );
        }
        

        $contentObject = $metadata->attribute( 'contentobject' );
        $contentObject->appendInputRelationList( $parser->getEmbeddedObjectIDArray(), eZContentObject::RELATION_EMBED );
        $contentObject->appendInputRelationList( $parser->getLinkedObjectIDArray(), eZContentObject::RELATION_LINK );
        */

        $xmlString = eZXMLTextType::domString( $document );

        $metadata->setAttribute( 'value', $xmlString );
        $metadata->store();
        return true;
    }
    
    protected static function validateAndStoreNoEditorInput( $value, $metadata )
    {
        $http = eZHTTPTool::instance();
        $contentObjectID = $metadata->attribute( 'contentobject_id' );
        $value = $http->postVariable( 'Metadata_data_text_0' );

        $value = preg_replace('/\r/', '', $value);
        $value = preg_replace('/\t/', ' ', $value);

        // first empty paragraph
        $value = preg_replace('/^\n/', '<p></p>', $value );

        $parser = new eZOEInputParser( $contentObjectID, true, eZXMLInputParser::ERROR_ALL, true );
        $document = $parser->process( $value );

        if ( !is_object( $document ) )
        {
            throw new Exception( ezpI18n::tr( 'ismetadata/oemetadataxmlinput', 'Value is malformated, some tag may be missing!' ) );
        }

        $xmlString = eZXMLTextType::domString( $document );

        /*
        $urlIDArray = $parser->getUrlIDArray();

        if ( count( $urlIDArray ) > 0 )
        {
            $this->updateUrlObjectLinks( $contentObjectAttribute, $urlIDArray );
        }

        $contentObject = $contentObjectAttribute->attribute( 'object' );
        $contentObject->appendInputRelationList( $parser->getRelatedObjectIDArray(), eZContentObject::RELATION_EMBED );
        $contentObject->appendInputRelationList( $parser->getLinkedObjectIDArray(), eZContentObject::RELATION_LINK );
        */

        $metadata->setAttribute( 'value', $xmlString );
        $metadata->store();
        return eZInputValidator::STATE_ACCEPTED;
    }
    
    function attribute( $name )
    {
        if ( $name === 'aliased_handler' )
        {
            return array( 'edit_template_name' => 'ezxmltext', 'input_xml' => html_entity_decode( $this->attribute( 'input_xml' ) ) );
        }
        else
        {
            return parent::attribute( $name );
        }
    }
    
}

?>
