<?php
// Created on: <03-Aug-2009 15:08 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates
class ISMetaDataFunctionCollection
{
    static public function fetchCustomMetadata( $objectID )
    {
        $array =  ISMetaData::fetchExistingMetadatasByObjectId( $objectID );
        $result = array( 'result' => $array );
        return $result;
    }
    
    static public function fetchMetadataByObjectId( $objectID, $languageCode )
    {
        $array = ISMetaData::fetchList( $objectID, $languageCode );
        $result = array( 'result' => $array );
        return $result;
    }
}

?>
