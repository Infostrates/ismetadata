<?php
// Created on: <27-Jul-2009 14:38 Marc Sallaberremborde>
//
// SOFTWARE NAME: IS Metadata
// SOFTWARE RELEASE: 1
// COPYRIGHT NOTICE: Copyright (C) 2009 Infostrates

class ISMetaData extends eZPersistentObject
{
    /**
     * Pour référence :
     * @param $row
     * @return void
     */
    function eZMetaData( $row )
    {
        $this->eZPersistentObject( $row );
    }

    /**
     * Pour référence :
     * @return array
     */
    static function definition()
    {
        return array( "fields" => array( 'contentobject_id' => array( 'name' => 'ContentObjectId',
                                                                      'datatype' => 'integer',
                                                                      'default' => 0,
                                                                      'required' => true,
                                                                      'foreign_class' => 'eZContentObject',
                                                                      'foreign_attribute' => 'id',
                                                                      'multiplicity' => '1..*' ),
                                         'language_code' => array( 'name' => 'LanguageCode',
                                                                   'datatype' => 'string',
                                                                   'default' => 'eng-GB',
                                                                   'required' => true ),
                                         'name' => array( 'name' => 'Name',
                                                          'datatype' => 'string',
                                                          'default' => '',
                                                          'required' => true ),
                                         'value' => array( 'name' => 'Value',
                                                           'datatype' => 'string',
                                                           'default' => '',
                                                           'required' => true ) ),
                      'keys' => array( 'contentobject_id', 'language_code', 'name' ),
                      'function_attributes' => array( 'content' => 'content',
                                                      'is_custom' => 'isCustom',
                                                      'contentobject' => 'contentobject',
                                                      'currents' => 'currents',
                                                      'nearest_parent' => 'nearestParent',
                                                      'default_content' => 'defaultContent',
                                                      'attribute_content' => 'attributeContent',
                                                      'custom_content' => 'customContent',
                                                      'public_name' => 'publicName',
                                                      'is_xml_block' => 'isXmlBlock',
                                                      'language_name' => 'languageName',
                                                      'origins' => 'origins',
                                                      'heritage_sources' => 'heritageSources' ),
                      'class_name' => 'ISMetaData',
                      'name' => 'ismetadata' );
    }

    /**
     * Récupérérer tout les métas d'un objet pour une langue donnée.
     * @param $objectID int Identifiant de l'objet concerné
     * @param $languageCode string Language code demandé (ex : eng-GB)
     * @param $asObject bool
     * @return array
     */
    static function fetchList( $objectID, $languageCode, $asObject = true )
    {
        $array = self::fetchObjectList( ISMetaData::definition(),
                                        null,
                                        array( "contentobject_id" => $objectID, "language_code" => $languageCode ),
                                        null,
                                        null,
                                        $asObject );
        $result = array();
        if ( $array )
        {
            foreach( $array as $val )
            {
                $result[$val->attribute( 'name' )] =  $val;
            }
        }
        $return = array();
        $keyList = eZINI::instance( 'ismetadata.ini' )->variable( 'MetaData', 'AvailablesMetaData' );
        foreach( $keyList as $key )
        {
            if ( isset( $result[$key] ) ) 
            {
                $return[$key] = $result[$key];
            }
            else
            {
                $return[$key] = new ISMetaData( array( 'contentobject_id' => $objectID, 'language_code' => $languageCode, 'name' => $key ) );
            }
        }
        return $return;
    }

    
    /**
     * Tente de retourner les objets métadata courant dans un tableau d'objets ISMetaData
     * @param $asObject bool
     * @return array
     */
    public static function currents( $asObject = true )
    {
        if ( self::$_currentsMetadatas === null)
        {
            $moduleResult = eZTemplate::instance()->variable( 'module_result');
            if ( isset( $moduleResult ) && isset( $moduleResult['node_id'] ) ) {
                self::$_currentsMetadatas = self::fetchList( eZContentObjectTreeNode::fetch( $moduleResult['node_id'] )->attribute( 'contentobject_id' ), eZLocale::currentLocaleCode(), $asObject );
            }
            else
            {
                self::$_currentsMetadatas = self::fetchList( eZContentObjectTreeNode::fetch( 2 )->attribute( 'contentobject_id' ), eZLocale::currentLocaleCode(), $asObject );
            }
        }
        return self::$_currentsMetadatas;
    }

    /**
     * Tente de retourner l'objet métadata courant de la propriété dont le nom est en paramètre
     * @param $name string
     * @return ISMetaData
     */
    public static function currentByName( $name )
    {
        $currents = self::currents();
        if ( isset( $currents[$name] ) )
        {
            return $currents[$name];
        }
        return false;
    }

    /**
     * Suppression pure et simple te toute la base de données des métadata
     * @return unknown_type
     */
    static public function purgeAll()
    {
        $db = eZDB::instance();
        $db->query( "DELETE FROM ismetadata" );
    }

    /**
     * Renvoi le contenu HTML de la meta-data à afficher.
     * Affiche donc la méta-data par défaut s'il n'y a pas de valeurs
     * @return string
     */
    public function content()
    {
        if ( $this->_content === null )
        {
            eZDebug::accumulatorStart( 'ismetadata_content', 'ISMetadata', 'Content Generation' );
            $ini = eZINI::instance( 'ismetadata.ini' );
            $customContent = $this->customContent();
            $attributeContent = false;
            $defaultContent = false;
            
            
            if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'SourceAttributeAction' ) )
            {
                $sourceAttributeAction = $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'SourceAttributeAction' );
            }
            else
            {
                $sourceAttributeAction = 'fallback';
            }
            if ( !$customContent || empty( $sourceAttributeAction ) || in_array( $sourceAttributeAction, array( 'append', 'prepend', 'override' ) ) )
            {
                $attributeContent = $this->attributeContent();
                if ( $sourceAttributeAction === 'override' ) $customContent = false;
            }
            if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'Action' ) )
            {
                $action = $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'Action' );
            }
            else
            {
                $action = 'override';
            }
            if ( ( empty( $customContent ) && empty( $attributeEvent ) ) || in_array( $action, array( 'prepend', 'append' ) ) )
            {
                $defaultContent = $this->defaultContent();
            }
            
            if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'MergeString' ) )
            {
                $mergeString = $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'MergeString' );
            }
            else
            {
                $mergeString = ' ';
            }
            
            $origins = array();
            $content = $customContent;
            if ( $customContent )
            {
                $origins[] = 'custom';
            }
            if ( $attributeContent !== false )
            {
                switch( $attributeContent )
                {
                    case 'prepend':
                        $content = implode( $mergeString, array( $attributeContent, $content ) );
                        $origins[] = 'attribute';
                        break;
                    case 'append':
                        $content = implode( $mergeString, array( $content, $attributeContent ) );
                        $origins[] = 'attribute';
                        break;
                    case 'override' :
                        $content = $attributeContent;
                        $origins = array( 'attribute' );
                        break;
                    case 'fallback' :
                    default :
                    {
                        if ( empty( $content ) )
                        {
                            $content = $attributeContent;
                            if ( !empty( $content ) )
                            {
                                $origins[] = 'attribute';
                            }
                        }
                    }
                }
            }
            if ( $defaultContent !== false )
            {
                if ( empty( $content ) )
                {
                    $content = $defaultContent;
                    $origins[] = 'default';
                }
                else
                {
                    switch( $action )
                    {
                        case 'prepend' :
                        {
                            $content = implode( $mergeString, array( $content, $defaultContent ) );
                            $origins[] = 'default';
                            break;
                        }
                        case 'append' :
                        {
                            $content = implode( $mergeString, array( $defaultContent, $content ) );
                            $origins[] = 'default';
                            break;
                        }
                    }
                }
            }
            $this->_origins = $origins;
            if ( $this->attribute( 'name' ) === 'title' )
            {
                $content = $this->pathAdditionForTitle( $content );
            }
            $this->_content = $content;
            eZDebug::accumulatorStop( 'ismetadata_content' );
        }
        return $this->_content;
    }

    /**
     * Détermine s'il s'agit d'un méta data par défaut ou personnalisé.
     * @return bool
     */
    public function isCustom()
    {
        $value = $this->attribute( 'value' );
        if ( !empty( $value ) ) return true;
        return false;
    }

    /**
     * Renvoi le fichier impacté par ce métadata
     * @return eZContentObject
     */
    public function contentObject()
    {
        return eZContentObject::fetch( $this->attribute( 'contentobject_id' ) );
    }

    /**
     * Retourne le metadata parent le plus proche (par rapport au noeud actuel si possible, sinon par rapport au noeud principal de l'objet courant) de même nom et langue
     * @return ISMetaData|bool
     */
    public function nearestExistingParent()
    {
        if ( $this->_nearestParent === null )
        {
            $templateVariables = eZTemplate::instance()->Variables[''];
            if ( !empty( $templateVariables ) && isset( $templateVariables['node'] ) && is_object( $templateVariables['node'] ) && $templateVariables['node']->attribute( 'contentobject_id' ) === $this->attribute( 'contentobject_id' ) )
            {
                $pathString = $templateVariables['node']->attribute( 'path_string' );
            }
            else
            {
                $pathString = $this->contentObject()->attribute( 'main_node' )->attribute( 'path_string' );
            }
            $path = explode( '/', $pathString );
            $path = array_splice( $path, 1, ( count( $path ) - 3 ) );
            $db = eZDB::instance();
            $query = 'SELECT ismetadata.*, ezcontentobject_tree.node_id as source_node_id 
                      FROM ismetadata 
                      LEFT JOIN ezcontentobject ON ismetadata.contentobject_id = ezcontentobject.id 
                      LEFT JOIN ezcontentobject_tree ON ezcontentobject_tree.contentobject_id = ezcontentobject.id 
                                AND ezcontentobject_tree.node_id = ezcontentobject_tree.main_node_id
                      WHERE ezcontentobject_tree.node_id IN ( '.implode( ', ', $path ). ')
                            AND ismetadata.name = "'.$db->escapeString( $this->attribute( 'name' ) ).'"
                            AND ismetadata.language_code = "'.$db->escapeString( $this->attribute( 'language_code' ) ).'"
                      ORDER BY ezcontentobject_tree.depth DESC';
            
            $array = $db->arrayQuery( $query, array( 'limit' => 1 ) );
            if ( empty( $array ) )
            {
                $this->_nearestParent = false;
            }
            else
            {
                $this->_heritageSources['custom'] = $array[0]['source_node_id'];
                $this->_nearestParent = new ISMetaData( $array[0] );
            }
        }
        return $this->_nearestParent;
    }
    
    /**
     * Récupère le contenu de l'attribut significatif pour ce metadata du parent le plus proche (par rapport au noeud actuel si possible, sinon par rapport au noeud principal de l'objet courant) de même nom et de même langue du même  
     * @return array Contenus de l'attribut
     */
    public function nearestAttributeContentParent( $pathString = false )
    {
        if ( $this->_nearestAttributeContentParent === null )
        {
            $this->_nearestAttributeContentParent = false;
            $templateVariables = eZTemplate::instance()->Variables[''];
            if ( !empty( $templateVariables ) && isset( $templateVariables['node'] ) && is_object( $templateVariables['node'] ) && $templateVariables['node']->attribute( 'contentobject_id' ) === $this->attribute( 'contentobject_id' ) )
            {
                $pathString = $templateVariables['node']->attribute( 'path_string' );
            }
            else
            {
                $pathString = $this->contentObject()->attribute( 'main_node' )->attribute( 'path_string' );
            }
            $path = explode( '/', $pathString );
            $path = array_splice( $path, 1, ( count( $path ) - 3 ) );
            $path = array_reverse( $path );
            foreach ( $path as $item )
            {
                $attributeContentItem = $this->attributeContentByObjectId( eZContentObjectTreeNode::fetch( $item )->attribute( 'contentobject_id' ) );
                if ( !empty( $attributeContentItem ) )
                {
                    $this->_heritageSources['attribute'] = $item;
                    $this->_nearestAttributeContentParent = $attributeContentItem;
                    break;
                }
            }
        }
        return $this->_nearestAttributeContentParent;
    }
    
    /**
     * Renvoi la valeur par défaut du métadata
     * @return string
     */
    public function defaultContent()
    {
        if ( $this->_defaultContent === null )
        {
            $ini = eZINI::instance( 'ismetadata.ini' );
            if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'DefaultIniVariable' ) )
            {
                $defaultIniVariable = $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'DefaultIniVariable' );
            }
            else
            {
                $defaultIniVariable = false;
            }
            $this->_defaultContent = '';
            if ( !empty( $defaultIniVariable ) ) 
            {
                $array = explode( '/', $defaultIniVariable );
                $value = ezini::instance( $array[0] )->variable( $array[1], $array[2] );
                if ( is_array( $value ) )
                {
                    if ( isset( $array[3] ) && isset( $value[$array[3]] ) )
                    {
                        $this->_defaultContent = $value[$array[3]];
                    }
                } 
                else
                {
                    $this->_defaultContent = $value;
                }
            }
        }
        return $this->_defaultContent;
    }

    /**
     * Récupération de l'éventuel contenu provenant des attributs de l'objets
     * @return string
     */
    public function attributeContent()
    {
        if ( $this->_attributeContent === null )
        {
            $values = array();
            $contentAttributeValues = $this->attributeContentByObjectId( $this->attribute( 'contentobject_id' ) );
            if ( !empty( $contentAttributeValues ) )
            {
                //Traitement des datatype spéciaux
                foreach( $contentAttributeValues as $contentAttributeValue )
                {
                    if ( !empty( $contentAttributeValue ) )
                    {
                        $values[] = $contentAttributeValue;
                    }
                }
            }
            
            $ini = eZINI::instance( 'ismetadata.ini' );
            if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'MergeString' ) )
            {
                $mergeString = $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'MergeString' );
            }
            else
            {
                $mergeString = ' ';
            }
            
            //Héritage
            if ( empty( $values ) )
            {
                if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'Heritage' ) )
                {
                    $heritage = ( strtolower( $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'Heritage' ) ) === 'enabled' );
                }
                else
                {
                    $heritage = false;
                }
                if ( $heritage )
                {
                    $nearestAttributeContent = $this->nearestAttributeContentParent();
                    if ( !empty( $nearestAttributeContent ) )
                    {
                        $values = $this->nearestAttributeContentParent();
                    }
                }
            }

            $value = implode( $mergeString, $values );

            $this->_attributeContent = $value;
        }
        return $this->_attributeContent;
    }
    
    /**
     * Récupération de l'éventuel contenu personnalisé 
     * @return string
     */
    public function customContent(  )
    {
        if ( $this->_customContent === null )
        {
            $xmlBlock = $this->isXmlBlock();
            if ( $xmlBlock )
            {
                $value = $this->xmlBlockContent();
            }
            else
            {
                $value = $this->attribute( 'value' );
            }
            
            if ( empty( $value ) )
            {
                
                $ini = eZINI::instance( 'ismetadata.ini' );
                if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'Heritage' ) )
                {
                    $heritage = ( strtolower( $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'Heritage' ) ) === 'enabled' );
                }
                else
                {
                    $heritage = false;
                }
                if ( $heritage && $this->nearestExistingParent() )
                {
                    $value = $this->nearestExistingParent()->customContent();
                }
            }
            $this->_customContent = $value;
        }
        return $this->_customContent;
    }
    
    /**
     * Retourne le contenu HTML du contenu en mode XMLBlock 
     * @return string
     * @TODO Méthode à tester.
     */
    protected function xmlBlockContent()
    {
        $value = $this->attribute( 'value' );
        $outputHandler = new eZXHTMLXMLOutput( $value, false );
        return $outputHandler->outputText();
    }
    
    /**
     * Créé un objet métadata et le stock en base de donnée.
     * Renvoi une exception en cas de problème.
     * @param $objectId int
     * @param $name string
     * @param $languageCode string
     * @param $value string
     * @return void
     */
    public static function create( $objectId, $name, $languageCode, $value )
    {
        if ( empty( $name ) )
        {
            throw new Exception( ezpI18n::tr( 'ismetadata/metadata', 'Please choose the name of the metadata you want to create.' ) );
        }
        if ( empty( $languageCode ) )
        {
            throw new Exception( ezpI18n::tr( 'ismetadata/metadata', 'Please choose a language for the metadata you want to create.' ) );
        }
        $availableName = eZINI::instance( 'ismetadata.ini' )->variable( 'MetaData', 'AvailablesMetaData' );
        if ( !in_array( $name, $availableName ) )
        {
            throw new Exception( ezpI18n::tr( 'ismetadata/metadata', 'Selected name is not an available meta name. Please check ismetadata.ini.' ) );
        }
        $metadata = self::fetchExisting( $objectId, $name, $languageCode );
        if ( $metadata )
        {
            throw new Exception( ezpI18n::tr( 'ismetadata/metadata', 'This meta exists in this language, please edit the existing one or change Name or Language.' ) );
        }
        $metadata = new ISMetaData( array( 'contentobject_id' => $objectId, 'name' => $name, 'language_code' => $languageCode ) );
        $metadata->setCustomContent( $value ); //This method include a store()
    }
    
    /**
     * Récupère un objet métadata déjà existant en base de donnée, ou renvoi 0.
     * @param $objectId int Identifiant d'objet
     * @param $name string Nom de la propriété
     * @param $languageCode string Language code
     * @return ISMetaData
     */
    public static function fetchExisting( $objectId, $name, $languageCode )
    {
        $db = eZDB::instance();
        $query = 'SELECT * 
                  FROM ismetadata 
                  WHERE contentobject_id = '.( int )$objectId.' 
                        AND name = "'.$db->escapeString( $name ).'" 
                        AND language_code = "'.$db->escapeString( $languageCode ).'"';
        $result = $db->arrayQuery( $query );
        if ( count( $result ) > 0 ) return new ISMetaData( $result[0] );
        return false;
    }

    /**
     * Récupère une liste d'objet métadata déjà existant en base de donnée.
     * @param $objectId int Identifiant d'objet
     * @return ISMetaData
     */
    public static function fetchExistingMetadatasByObjectId( $objectId )
    {
        $array = array();
        $db = eZDB::instance();
        $query = 'SELECT * 
                  FROM ismetadata 
                  WHERE contentobject_id = '.( int )$objectId;
        $result = $db->arrayQuery( $query );
        if ( count( $result ) > 0 ) {
            
            foreach( $result as $line )
            {
                $array[] = new ISMetaData( $line );
            }
        }
        return $array;
    }
    
    /**
     * Modifie la valeur personnalisé de l'objet courant et la stock en mémoire.
     * Renvoi une exception en cas de problème.
     * @param $value string Nouvelle valeur
     * @return void
     */
    public function setCustomContent( $value )
    {
        $db = eZDB::instance();
        $db->begin();
        try 
        {
            if ( $this->isXmlBlock() )
            {
                ISOEMetaDataXMLInput::validateAndStoreInput( $value, $this );
            }
            else
            {
                $this->setAttribute( 'value', $value );
                $this->store();
            }
        }
        catch (Exception $e)
        {
            $db->rollback();
            throw $e;
        }
        $db->commit();
    }
    
    /**
     * Supprime une liste de métadata fournie en paramêtre
     * @param $metadataList ISMetaData
     * @return void
     */
    public static function removeList( $metadataList )
    {
        $db = ezdb::instance();
        $db->begin();
        try 
        {
            foreach( $metadataList as $metadata )
            {
                $metadata->remove();
            }
        } catch (Exception $e)
        {
            $db->rollback();
            throw $e;
        }
        $db->commit();
    }
    
    /**
     * Détermine si ce metadata à une valeur personalisée en XMLBlock 
     *
     */
    public function isXmlBlock()
    {
        $ini = eZINI::instance( 'ismetadata.ini' );
        if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'XmlBlock' ) )
        {
            return ( strtolower( $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'XmlBlock' ) ) === 'enabled' );
        }
        return false;
    }
    
    /**
     * Retourne le nom "public" (de la vue utilisateur) d'une métadata (configuré dans le .ini)
     * @return string
     */
    public function publicName()
    {
        $ini = eZINI::instance( 'ismetadata.ini' );
        if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'Name' ) )
        {
            return $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'Name' );
        }
        return $this->attribute( 'name' );
    }
    
    /**
     * Retourne le nomde de la langue du metadata
     * @return string
     */
    public function languageName()
    {
        $contentLanguage = eZContentLanguage::fetchByLocale( $this->attribute( 'language_code' ) );
        if ( is_object( $contentLanguage ) )
        {
             return ( $contentLanguage->attribute( 'name' ) );
        }
        return false;
    }
    
    /**
     * Méthode d'ajout éventuel du path pour l'attribut titre. 
     * @param $normalContent string Contenu du metadata avant ce traitement
     * @return string Nouveau contenu
     */
    protected function pathAdditionForTitle( $normalContent )
    {
        $ini = eZINI::instance( 'ismetadata.ini' );
        $addPath = strtolower( $ini->variable( 'MetaDataTitleDefaultActions', 'AddPath' ) ); 
        if ( $addPath !== 'disabled' )
        {
            $add = false;
            switch( $addPath )
            {
                case 'enabled' :
                {
                    $add = true;
                    break;
                }
                case 'onattribute' :
                {
                    if ( !is_array( $this->_origins ) || $this->_origins[0] !== 'custom' )
                    {
                        $add = true;
                    }
                    break;
                }
                case 'ondefault' :
                {
                    if ( !is_array( $this->_origins ) || !in_array( $this->_origins[0],  array( 'custom', 'attribute' ), true ) )
                    {
                        $add = true;
                    }
                    break;
                }
            }
            if ( $add )
            {
                $path = self::path();
                if ( $path )
                {
                    $addMode = strtolower( $ini->variable( 'MetaDataTitleDefaultActions', 'AddMode' ) );
                    $mergeString = $ini->variable( 'MetaDataTitleDefaultActions', 'MergeString' );
                    switch( $addMode )
                    {
                        case 'prepend' :
                        {
                            $normalContent = $path.$mergeString.$normalContent;
                            break;
                        }
                        case 'append' :
                        default :
                        {
                            $normalContent = $normalContent.$mergeString.$path;
                            break;
                        }
                    }
                }
            }
        }
        return $normalContent;
    }
    
    /**
     * Détermine les origines du métadata courant. 
     * @return array de string (l'ordre n'a pas de signification au niveau de la chaine) pouvant être : custom, attribute, default 
     */
    public function origins()
    {
        if ( $this->_origins === null )
        {
            $this->content();
        }
        return $this->_origins;
    }
    
    /**
     * Tente de déterminer le path de l'object courant.
     * @return string
     */
    protected static function path()
    {
        $moduleResult = eZTemplate::instance()->variable( 'module_result');
        if ( !is_array( $moduleResult ) || !( !is_array( $moduleResult['path'] && isset( $moduleResult['title_path'] ) && !is_array( $moduleResult['title_path'] ) ) ) )
        {
            $array = eZTemplate::instance()->Variables[''];
            if ( isset( $array ) && is_array( $array ) )
            {
                if ( isset( $array['node_path'] ) )
                {
                    $moduleResult['path'] = $array['node_path'];
                }
                elseif ( isset( $array['node'] ) && is_object( $array['node'] ) )
                {
                    $moduleResult['path'] = self::pathByNodeId( $array['node']->attribute( 'node_id' ) );
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        if ( isset( $moduleResult['title_path'] ) && is_array( $moduleResult['title_path'] ) )
        {
            $path = $moduleResult['title_path'];
        }
        else
        {
            $path = $moduleResult['path'];
        }
        $array = array();
        if (  $path instanceof eZTemplateSectionIterator )
        {
            $pathNode = $path->InternalAttributes['item']->attribute( 'path' );
            foreach( $pathNode as $item )
            {
                $array[] = $item->attribute( 'name' );
            }
            $array[] = $path->InternalAttributes['item']->attribute( 'name' );
        }
        elseif ( isset( $path[0]['text'] ) )
        {
            foreach( $path as $item )
            {
                $array[] = $item['text'];
            }
        }
        $ini = eZINI::instance( 'ismetadata.ini' );
        $pathLimit = ( int )$ini->variable( 'MetaDataTitleDefaultActions', 'PathLimit' );
        if ( ( int )$pathLimit !== 0 )
        {
            $array = array_slice( $array, ( int )$ini->variable( 'MetaDataTitleDefaultActions', 'PathOffset' ), $pathLimit  );
        }
        else
        {
            $array = array_slice( $array, ( int )$ini->variable( 'MetaDataTitleDefaultActions', 'PathOffset' ) );            
        }
        if ( strtolower( $ini->variable( 'MetaDataTitleDefaultActions', 'ReversedPath' ) ) === 'true' )
        {
            $array = array_reverse( $array );
        }
        
        return implode(  $ini->variable( 'MetaDataTitleDefaultActions', 'PathSeparator' ), $array );
    }
    
    /**
     * Détermine un array de path simplifié à partir du nodeId de l'object courant.
     * @param $nodeId
     * @return array
     */
    protected static function pathByNodeId( $nodeId )
    {
        $array = array();
        $node = eZContentObjectTreeNode::fetch( $nodeId );
        if ( !$node ) return array();
        $pathNode = $node->fetchPath();
        foreach ( $pathNode as $node )
        {
            $array[] = array( 'text' => $node->attribute( 'name' ), 'url' => $node->attribute( 'url_alias' ) );
        }
        return $array;
    }
    
    /**
     * Récupère les contenus des attributs configurés pour servir de contenu dans le metadata
     * @param $objectId Identifiant de l'objet
     * @return array Tableau avec tout les contenus dse attributs de metadata
     */
    protected  function attributeContentByObjectId( $objectId )
    {
        $ini = eZINI::instance( 'ismetadata.ini' );
        
        if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'SourceAttributeName' ) )
        {
            $sourceAttributeName = $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'SourceAttributeName' );
        }
        else
        {
            $sourceAttributeName = false;
        }
        
        if ( $ini->hasVariable( 'MetaData_'.$this->attribute( 'name' ), 'SourceAttributesDataType' ) )
        {
            $sourceAttributesDataType = $ini->variable( 'MetaData_'.$this->attribute( 'name' ), 'SourceAttributesDataType' );
        }
        else
        {
            $sourceAttributesDataType = false;
        }
        
        
        $contentAttributes = array();
        if ( !empty( $sourceAttributeName ) || !empty( $sourceAttributesDataType ) )
        {
            $object = eZContentObject::fetch( $objectId );
            if ( $object )
            {
                $dataMap = $object->attribute( 'data_map' );
            }
        }
        
        if ( !empty( $sourceAttributeName ) )
        {
            if ( isset( $dataMap[$sourceAttributeName] ) )
            {
                $contentAttributes[] = self::contentObjectAttributeContent( $dataMap[$sourceAttributeName] );
            }
        }
        
        if ( !empty( $sourceAttributesDataType ) )
        {
            foreach( $dataMap as $attribute )
            {
                if ( $attribute->attribute( 'data_type_string' ) === $sourceAttributesDataType )
                {
                    $contentAttributes[] = self::contentObjectAttributeContent( $attribute );
                }
            }
        }
        return $contentAttributes;
    }

    /**
     * Renvoi les node_id des noeud ayant servi à l'héritage de custom et attribute. 
     * @return array de int (avec les index : custom, attribute ). La valeur est à false s'il n'y a pas eu héritage
     */
    public function heritageSources()
    {
        if ( $this->_heritageSources === null )
        {
            $this->content();
        }
        return $this->_heritageSources;
    }
    
    /**
     * Récupère la valeur d'un attribut en fonction de son datatype
     * Méthode à éditer selon vos besoins
     * @param $contentAttribute
     * @return
     */
    protected function contentObjectAttributeContent( $contentAttribute )
    {
        $contentAttributeValue = '';
        switch ( $contentAttribute->attribute( 'data_type_string' ) )
        {
            case 'ezkeyword' :
            {
                $contentAttributeValue = $contentAttribute->attribute( 'content' )->attribute( 'keyword_string' );
                break;
            }
            default :
            {
                $contentAttributeValue = $contentAttribute->attribute( 'content' );
            }
        }
        return $contentAttributeValue;
    }
    
    protected $_content = null;
    protected $_customContent = null;
    protected $_defaultContent = null;
    protected $_attributeContent = null;
    protected $_nearestParent = null;
    protected $_nearestAttributeContentParent = null;
    protected $_origins = null;
    protected $_heritageSources = array( 'custom' => false, 'attribute' => false );
    
    static protected $_currentsMetadatas = null;
}

?>
